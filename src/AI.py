import Board
from header import *
from random import randrange

class AI:
    def __init__(self, board: Board, team):
        self.board = board
        self.team = team


    def move(self):
        return


    def place(self):

        while(True):
            x = randrange(self.board.board_size[1]+1)
            y = randrange(self.board.board_size[1]+1)

            # Update board
            if self.check_placement((x,y)) and self.board.check_empty((x,y)):
                self.board.place_piece((x,y), self.team)
                break

        self.board.print_board()

        return x, y

    def check_placement(self, pos):

        if self.team == WHITE_PIECE:
            if pos[VERT] > 5:
                return False

        else:
            if pos[VERT] < 2:
                return False

        return True