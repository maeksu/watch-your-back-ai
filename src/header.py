"""
header.py contains many of the constants that are universal throughout the code. It also contains functions that
initialise the state of the program.

Dated Edited:


"""


import Square

# =========
# CONSTANTS
# =========
WHITE_PIECE = "O"
BLACK_PIECE = "@"
SPACE = "-"
CORNER = "X"


UP = (0, -1)
DOWN = (0, 1)
LEFT = (-1, 0)
RIGHT = (1, 0)

DIRECTIONS = [UP, DOWN, LEFT, RIGHT]

X = 0
Y = 1

VALID = 1
INVALID = 0
FLAG = 0
POSITION = 1

HOZ = 0
VERT = 1

MAX_DIST = 64

def make_board():
    # Reading and parsing game state / input
    path = "..\simple.txt"
    board_state = open(path, "r+")

    dict_board = {}

    x = 0
    y = 0

    nlines = 1

    for s in board_state:

        level_line = s.rstrip("\n").split(" ")

        if (nlines == 9):
            turn_state = level_line.pop()
            break
        nlines += 1
        for square in level_line:
            dict_board.update({(x, y): Square.Square(square, (x, y))})
            x += 1
        y += 1
        x = 0

    return dict_board, turn_state


def parse_board():
    """
    Creates a dictionary where
     - key = position
     - value = square object

    :param board:
    :return:
    """
    dict_board = {}

    x = 0
    y = 0

    nlines = 1
    while True:
        line_input = input()
        if (nlines == 9):
            turn_state = line_input
            break
        level_line = line_input.split(" ")  # list
        nlines += 1
        for square in level_line:
            dict_board.update({(x, y): Square.Square(square, (x, y))})
            x += 1
        y += 1
        x = 0

    return dict_board, turn_state

def print_board(board: dict):
    y = 0
    for square in board.values():
        if y != square.pos[Y]:
            y += 1
            print("\n")
        print(square.square_t, end=" ")
    print("\n")