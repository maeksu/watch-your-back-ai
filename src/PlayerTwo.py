from header import *
import AB
import Board
import RandomAI

PLACEMENT = 0
MOVEMENT = 1
FORFEIT = 2

MINE = 0
ENEMY = 1

# SHRINK_LIMIT = 64
SHRINK_TURNS = [128, 192]

class Player:

    # ========= STANDARDISED FUNCTIONS  ========= #
    def __init__(self, colour: str):
        pieces = self.determine_piece(colour)
        self.team = pieces[MINE]
        self.enemy_team = pieces[ENEMY]
        self.phase = PLACEMENT
        self.n_placements = 0
        self.n_turns = 0
        self.AI = AB.AB(Board.Board(), self.team, self.enemy_team, self.n_turns)

    def action(self, turns: int):
        self.n_turns = turns
        self.AI.n_turns = turns
        self.check_strink()
        """
        Outputs an action for opponent class (to referee).
        turns: a int representing the numbers of moves pasted. Odd is black, even is white.


        3 types of actions are possible. Represented below:

        Place: (x, y)
        Move: ((x, y), (x, y)) tuple1: piece -> tuple2: move here
        Forfeit: None

        :param turns: tunes is the number of turns pasted.
        :return:
        """
        print("MY BOARD")
        self.AI.board.print_board()

        # if self.team == WHITE_PIECE:
        print('turn starting, player =', self.team)
        if self.n_placements < 12:
            action = self.AI.place()
            self.n_placements += 1
        else:
            action = self.AI.move()

        # print(self.AI.seed)

        return action

    def update(self, action):
        self.AI.n_turns += 1
        self.n_turns += 1
        self.check_strink()
        """
        takes action from opponent (from referee) to update internal board. Input format is below:

        Place: (x, y)
        Move: ((x, y), (x, y)) tuple1: piece -> tuple2: move here
        Forfeit: None

        :param action:
        :return:
        """
        self.phase = self.determine_action(action)
        if self.phase == PLACEMENT:
            self.AI.board.place_piece(action, self.enemy_team)
        elif self.phase == MOVEMENT:
            piece_one = self.AI.board.get_square(action[0])
            piece_two = self.AI.board.get_square(action[1])

            self.AI.board.move_piece(piece_one, piece_two)


    # ========= OWN FUNCTIONS ========= #
    def determine_piece(self,colour):
        if colour == 'white':
            return WHITE_PIECE, BLACK_PIECE
        else:
            return BLACK_PIECE, WHITE_PIECE


    def determine_action(self, action):
        """
        Assumes input will always be correct. Will break if otherwise.

        Input (action) format expected:
        Place: (x, y)
        Move: ((x, y), (x, y)) tuple1: piece -> tuple2: move here
        Forfeit: None

        :param action:
        :return:
        """
        if action == None:
            return FORFEIT
        elif isinstance(action[0], int):
            return PLACEMENT
        else:
            return MOVEMENT


    def check_strink(self):
        if self.n_turns in SHRINK_TURNS:
            self.AI.board.shrink_board()

