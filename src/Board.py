"""
board.py is a class that represents the games board

Date Edited:


"""


from header import *
from math import sqrt

UPPER = 1
LOWER = 0

INIT_BOARD_SIZE = (0, 7)


class Board:

    def __init__(self, board=None):

        if board == None:
            self.board_dict = self.create_empty_board()
        else:
            self.board_dict = board

        self.board_size = self.set_board_size()
        self.list_pieces = []
        self.find_pieces()


    # ========= INSTANTIATERS ========= #
    def find_pieces(self):
        self.list_pieces = []
        for square in self.board_dict.values():
            if (square.square_t == WHITE_PIECE) or (square.square_t == BLACK_PIECE):
                self.list_pieces.append(square)

    def create_empty_board(self):
        board_dict = {}
        for y in range(INIT_BOARD_SIZE[1]+1):
            for x in range(INIT_BOARD_SIZE[1]+1):
                if x in INIT_BOARD_SIZE and y in INIT_BOARD_SIZE:
                    board_dict.update({(x, y): Square.Square(CORNER, (x, y))})
                else:
                    board_dict.update({(x, y): Square.Square(SPACE, (x, y))})

        return board_dict


    # ========= PIECE MANIPULATION (GETTER AND SETTERS) ========= #
    def get_square(self, pos):
        return self.board_dict[pos]


    def move_piece(self, piece_one, piece_two):
        piece_two.square_t = piece_one.square_t
        piece_one.square_t = SPACE
        self.find_pieces()
        self.update(piece_two.square_t)


    def place_piece(self, pos, new_square_t):
        location_piece = self.get_square(pos)
        location_piece.square_t = new_square_t
        self.find_pieces()
        self.update(new_square_t)


    def kill(self, piece):

        # piece.square_t = SPACE

        self.get_square(piece.pos).square_t = SPACE

        self.find_pieces()

    # ========= CHECKING FUNCTIONS ========= #
    def check_is_corner(self, pos):
        if pos[0] in self.board_size and pos[1] in self.board_size:
            return True
        else:
            return False


    def check_bounds(self, pos):
        if pos[X] < self.board_size[LOWER] or pos[X] > self.board_size[UPPER]:
            return False
        elif pos[Y] < self.board_size[LOWER] or pos[Y] > self.board_size[UPPER]:
            return False
        else:
            return True


    def check_death(self, piece, enemy_team = None):

        is_surround_hoz = True
        is_surround_vert = True

        if enemy_team == None:
            enemy_team = (BLACK_PIECE if piece.square_t == WHITE_PIECE else WHITE_PIECE)

        for direction in [LEFT, RIGHT]:
            new_pos = self.add_pos(piece.pos, direction)
            if self.check_bounds(new_pos) == False:
                is_surround_hoz = False
                break
            other_piece = self.get_square(new_pos).square_t
            if other_piece != CORNER and other_piece != enemy_team:
                is_surround_hoz = False

        for direction in [UP, DOWN]:
            new_pos = self.add_pos(piece.pos, direction)

            if self.check_bounds(new_pos) == False:
                is_surround_vert = False
                break
            other_piece = self.get_square(new_pos).square_t
            if other_piece != CORNER and other_piece != enemy_team:
                is_surround_vert = False


        return is_surround_hoz, is_surround_vert


    def check_empty(self,pos):
        if self.get_square(pos).square_t == SPACE:
            return True
        else:
            return False


    # ========= OUTPUT FUNCTIONS ========= #
    def print_board(self):

        for y in range(INIT_BOARD_SIZE[UPPER]+1):
            for x in range(INIT_BOARD_SIZE[UPPER]+1):

                if y in range(self.board_size[LOWER], self.board_size[UPPER]+1) and\
                    x in range(self.board_size[LOWER], self.board_size[UPPER]+1):
                    print(self.get_square((x,y)).square_t, end=" ")

                else:
                    print(" ", end=" ")

            print("")
        print("")



    # ========= MISCELLANEOUS ========= #
    def add_pos(self, pos1, pos2):
        return tuple(map(sum, zip(pos1, pos2)))

    def update(self, team_two = WHITE_PIECE):
        team_one = (BLACK_PIECE if team_two == WHITE_PIECE else WHITE_PIECE)

        # check pieces on the team that didn't just move, as #
        #  their pieces die first in a double-backstab       #
        team_one_list = self.filter_pieces(team_one)
        for piece in team_one_list:
            surrounded = self.check_death(piece)
            if surrounded[HOZ] or surrounded[VERT]:
                self.kill(piece)

        # now check pieces on the team that has just moved, #
        #  in case they killed their own piece              #
        team_two_list = self.filter_pieces(team_two)
        for piece in team_two_list:

            surrounded = self.check_death(piece)
            if surrounded[HOZ] or surrounded[VERT]:
                self.kill(piece)


    def filter_pieces(self, team):
        filtered = []

        for piece in self.list_pieces:
            if piece.square_t == team:
                filtered.append(piece)

        return filtered


    def shrink_board(self):

        self.board_size = (self.board_size[LOWER] + 1, self.board_size[UPPER] - 1)
        temp_board_dict = {}
        x = 0
        y = 0
        # row 0, colume 0, 
        # row 7[self.board_size[1]], colume 7[self.board_size[1]] 
        # deleted...
        for y in range(self.board_size[LOWER], self.board_size[UPPER]+1):
            for x in range(self.board_size[LOWER], self.board_size[UPPER]+1):
                square = self.get_square((x, y))
                if self.check_is_corner((x, y)):
                    temp_board_dict.update({(x, y): Square.Square(CORNER, (x, y))})
                else:
                    temp_board_dict.update({(x, y): Square.Square(square.square_t, (x, y))})

        self.board_dict.clear()
        self.board_dict.update(temp_board_dict)
        self.find_pieces()
        self.update()

    def set_board_size(self):
        # TODO: Make better
        l = sqrt(len(self.board_dict))
        if l == 8:
            return 0, 7
        if l == 6:
            return 1, 6
        if l == 4:
            return 2, 5
