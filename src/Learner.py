
import csv
from header import *

TEAM_FLAG = 0
PLACE     = 1
MOVE_FROM = 1
MOVE_TO   = 2

class Learner():

    def __init__(self):

        self.placements = []
        self.made_moves = []
        self.winner = 'Draw'

    def save_placement(self, piece_type, placement):

        self.placements.append([piece_type, placement])

    def save_move(self, piece_type, move):

        self.made_moves.append([piece_type, move[0], move[1]])

    def save_win(self, winning_piece):
        self.winner = winning_piece

    def log_all(self):

        with open('log.csv', 'r', newline='') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                pass
            if row[0].isdigit():
                gamenum = int(row[0]) + 1
            else:
                gamenum = 0

        with open('log.csv', 'a', newline='') as csvfile:
            writer = csv.writer(csvfile)

            for saved_placement in self.placements:
                writer.writerow([gamenum, saved_placement[TEAM_FLAG],
                saved_placement[PLACE], '', '', self.winner])

            for saved_move in self.made_moves:
                writer.writerow([gamenum, saved_move[TEAM_FLAG], '',
                  saved_move[MOVE_FROM], saved_move[MOVE_TO], self.winner])
