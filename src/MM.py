import AI
import Minimax
import Board
from tree import Tree, Node
from header import *
from copy import deepcopy


class MM(AI.AI):

    def __init__(self, board, team, enemy):
        super().__init__(board, team)
        self.enemy = enemy
        self.game_tree = Tree.Tree(self.board) # Do I want the same reference?
        self.minimax = Minimax.Minimax()


    def move(self):

        move = self.minimax.minimax(self.game_tree.root, 2, self.team)

        return move.move

