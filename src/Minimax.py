from sys import maxsize
from tree import Tree, Node
from header import *
from copy import deepcopy
import Player
import AlphaBeta
import Board


INF = maxsize


class Minimax(AlphaBeta.AlphaBeta):

    def minimax(self, node, depth, team):
        best_value = self._max_value(node, depth, team)  # ROOT

        successors = node.children

        if len(successors) == 0:
            return None

        # TODO: Remove before submit
        print("MiniMax:  Utility Value of Root Node: = " + str(best_value))

        best_move = None
        for state in successors:
            # print(best_value)
            if state.MM_value == best_value:
                best_move = state
                break

        return best_move

    def _max_value(self, node, depth, team):
        # TODO: Remove before submit
        print("MiniMax-->MAX: Visited Node :: " + str(node.board))

        # Setting -infinite
        max_value = -INF

        if depth == 0: # if depth is 0
            return node.h_value

        # create children here
        self._generate_moves(node, team)
        successor_states = node.children

        for state in successor_states:
            max_value = max(max_value, self._min_value(state, depth - 1, self._flip_team(team)))

        node.MM_value = max_value

        return max_value
