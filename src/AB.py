import AI
import Minimax
import Board
import AlphaBeta
from tree import Tree, Node
from header import *
from copy import deepcopy
from random import randrange
import Evaluate

DEPTH = 2


class AB (AI.AI):

    def __init__(self, board, team, enemy, n_turns):
        super ().__init__(board, team)
        self.enemy = enemy
        self.n_turns = n_turns
        self.alphabeta = AlphaBeta.AlphaBeta(self.team)

    def move(self):

        game_tree = Tree.Tree (Node.Node(self.board, None, None, self.team, self.n_turns))
        move = self.alphabeta.alphabeta_search(game_tree.root, DEPTH, self.team)

        if move == None:
            return None

        piece_one = self.board.get_square(move.move[0])
        piece_two = self.board.get_square(move.move[1])

        # Move internal board
        self.board.move_piece(piece_one, piece_two)

        # TODO: Remove this comment before submission
        # self.board.print_board()
        print(game_tree.root)
        print("AB value: " + str(game_tree.root.MM_value))

        return move.move

    def place(self):

        # while(True):

        #     x = randrange(self.board.board_size[1]+1)
        #     y = randrange(self.board.board_size[1]+1)

            # # Update board
            # if self.check_placement((x,y)) and self.board.check_empty((x,y)):
            #     self.board.place_piece((x,y), self.team)
            #     break
            
        #     self.find_available_center()

        # self.board.print_board()

        place = self.find_available_center()

        # Update board
        self.board.place_piece(place, self.team)
        return place


    def find_available_center(self):

        min_available_dist = MAX_DIST
        output_pos = (3,4)

        for square in self.board.board_dict:

            if self.check_placement(square) and self.board.check_empty(square):

                dist_from_center = min([Evaluate.find_distance(square, x) for x in Evaluate.CENTER])
                if dist_from_center < min_available_dist:
                    min_available_dist = dist_from_center
                    output_pos = (square)

        return output_pos

        


