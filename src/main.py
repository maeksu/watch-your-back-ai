"""
main.py runs the code
"""


import Player
import Board
import MM
import AB
from tree import Tree, Node
from header import *


TURN_MOVE = "Moves"
TURN_MASSACRE = "Massacre"
BOARD = 0
TURN_STATE = 1

"""
# == MAIN ==

parsed_input = header.make_board()

obj_board = Board.Board(parsed_input[BOARD])
turn_state = parsed_input[TURN_STATE]
print("==== INITIAL BOARD STATE ====\n")
header.print_board(obj_board.dict_board)
print("TURN STATE: ", turn_state)

AI_1 = Dave.Dave(obj_board)


if turn_state == TURN_MOVE:
    print(AI_1.count_moves(header.WHITE_PIECE))
    print(AI_1.count_moves(header.BLACK_PIECE))
elif turn_state == TURN_MASSACRE:
    AI_1.massacre()
"""

test_board = make_board()[0]

AI = MM.MM(Board.Board(test_board), WHITE_PIECE, BLACK_PIECE)
AI2 = AB.AB(Board.Board(test_board), WHITE_PIECE, BLACK_PIECE, 1)

print(AI2.move())
print(AI2.move())
