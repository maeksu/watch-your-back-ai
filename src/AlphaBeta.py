from sys import maxsize
from tree import Tree, Node
from header import *
from copy import deepcopy
import Player

import Board


INF = maxsize


class AlphaBeta:

    def __init__(self, team=None):
        self.team = team

    def alphabeta_search(self, node, depth, team):
        alpha = -INF
        beta = INF

        best_value = self._max_value(node, depth, team, alpha, beta)

        successors = node.children

        if len(successors) == 0:
            return None

        best_move = None
        for state in successors:
            # print(best_value)
            if state.MM_value == best_value:
                best_move = state
                break

        return best_move

    def _max_value(self, node, depth, team, alpha, beta):
        # TODO: Remove this before submission
        # print ("AlphaBeta-->MAX: Visited Node :: " + str (node.board))

        # Setting infinite
        value = -INF

        if depth == 0: # if depth is 0
            return node.h_value

        # create children here
        self._generate_moves(node, team, beta, False)
        successor_states = node.children

        for state in successor_states:
            value = max(value, self._min_value(state, depth - 1, self._flip_team(team), alpha, beta))
            if value >= beta:
                node.MM_value = value
                # print(value)
                return value
            alpha = max(alpha, value)

        node.MM_value = value

        # print(value)
        return value

    def _min_value(self, node, depth, team, alpha, beta):
        # TODO: Remove before submission
        # print("AlphaBeta-->MIN: Visited Node :: " + str(node.board))

        # Setting infinite
        value = INF

        if depth == 0:
            return node.h_value

        self._generate_moves (node, team, alpha, True)
        successor_states = node.children

        for state in successor_states:
            value = min(value, self._max_value(state, depth - 1, self._flip_team(team), alpha, beta))
            if value <= alpha:
                node.MM_value = value
                return value
            beta = min(beta, value)

        node.MM_value = value

        return value

    def _generate_moves(self, node, team, cutoff, is_alpha):
        shrink = False

        if node.n_turns + 1 in Player.SHRINK_TURNS:
            shrink = True

        # For board make every possible move set
        # for piece in node.board.filter_pieces(team): # List of pieces
        #     for direction in DIRECTIONS:
        #         new_pos = node.board.add_pos(piece.pos, direction)
        #         if node.board.check_bounds(new_pos) and node.board.check_empty(new_pos):
        #             self._new_state(node, piece, new_pos, shrink)

        for piece in node.board.filter_pieces(team):
            for direction in DIRECTIONS:
                poss_move = self.check_move(node.board, piece.pos, direction)
                if poss_move[FLAG]:
                    h_value = self._new_state(node, piece, poss_move[POSITION], shrink)
                    if is_alpha and h_value <= cutoff:
                        return
                    if not is_alpha and h_value >= cutoff:
                        return

    def _new_state(self, parent_node, piece, new_pos, shrink):
        """
        Creates a new node representing a new game state.

        :param parent_node: Parent node of new state
        :param piece: Selected piece to move
        :param new_pos: New position for selected piece
        :return:
        """

        new_dict = {}

        for key, value in parent_node.board.board_dict.items():
            new_dict.update({key: deepcopy(value)})

        new_board = Board.Board(new_dict)

        # TODO: Remove
        assert new_board.check_bounds(new_pos) and new_board.check_bounds(piece.pos) , "POS OR NEW-POS OUT OF BOUNDS"

        piece_one = new_board.get_square (piece.pos)
        piece_two = new_board.get_square (new_pos)

        team = self.team

        new_board.move_piece(piece_one, piece_two)


        if shrink:
            new_board.shrink_board()

        node = parent_node.add_node(Node.Node(new_board, parent_node, (piece_one.pos, piece_two.pos), team, parent_node.n_turns + 1))

        return node.h_value

    def _flip_team(self, team):
        if team == WHITE_PIECE:
            return BLACK_PIECE
        else:
            return WHITE_PIECE

    def check_move(self, board, pos, direction):

        # checks if move is valid
        new_pos = board.add_pos(pos, direction)
        if board.check_bounds(new_pos) and board.board_dict.get(new_pos).square_t == SPACE:
            return True, new_pos

        jumped_pos = board.add_pos(new_pos, direction)
        if board.check_bounds(jumped_pos) and board.board_dict.get(jumped_pos).square_t == SPACE:
            return True, jumped_pos
                    
        return False, pos
