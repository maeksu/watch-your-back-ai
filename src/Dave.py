"""
dave.py contains all the necessary tools for the AI to work. Makes decisions and judgements that humans would make

Version: 1.0
Date Edited:

TODO:
    kill orientation
    suicide prevention (in find_new_moves)
        - can move if it kills one of the pieces however...



"""

import Route
from Board import *
import header

# constants #
HOZ = 0
VERT = 1
SCORE = 0
PATH = 1

VALID_FLAG = 0

# PART A #
class Dave:

    def __init__(self, board):
        self.board = board

    def count_moves(self, piece_type):
        # Totals amount of moves by piece type #

        total_moves = 0
        for piece in self.board.list_pieces:
            if piece.square_t == piece_type:
                for direction in header.DIRECTIONS:
                    if self.check_move(piece.pos, direction)[VALID_FLAG] is True:
                        total_moves += 1

        return total_moves


    def check_move(self, pos, direction):

        # failsafe; check direction is valid #
        if direction not in header.DIRECTIONS:
            # TODO: Proper Error Exception #
            print("ERROR: WRONG DIRECTION")
            return None

        # checks if move is valid
        """
        new_pos = self.add_pos(pos, direction)
        if self.board.within_board(new_pos) and self.board.dict_board.get(new_pos).square_t == header.SPACE:
            next_piece = self.board.get_square(new_pos)
            is_surrounded = self.board.check_death(next_piece, header.BLACK_PIECE)
            if not is_surrounded[HOZ] and not is_surrounded[VERT]:
                return True, new_pos
            jumped_pos = self.add_pos(new_pos, direction)
        if self.board.within_board(jumped_pos) and self.board.dict_board.get(jumped_pos).square_t == header.SPACE:
             jumped_piece = self.board.get_square(jumped_pos)
                 is_surrounded = self.board.check_death(jumped_piece, header.BLACK_PIECE)
             if not is_surrounded[HOZ] and not is_surrounded[VERT]:
                 return True, jumped_pos
        else:
                return False, pos
        """

        new_pos = self.add_pos(pos, direction)
        # TODO: this suicide check is shit fix it
        if self.board.check_bounds(new_pos) and self.board.dict_board.get(new_pos).square_t == header.SPACE and not\
            self.board.check_death(self.board.get_square(new_pos), header.BLACK_PIECE)[HOZ] and not \
            self.board.check_death(self.board.get_square(new_pos), header.BLACK_PIECE)[VERT]:
            return True, new_pos
        jumped_pos = self.add_pos(new_pos, direction)
        if self.board.check_bounds(jumped_pos) and self.board.dict_board.get(jumped_pos).square_t == header.SPACE and not\
            self.board.check_death(self.board.get_square(jumped_pos), header.BLACK_PIECE)[HOZ] and not \
            self.board.check_death(self.board.get_square(jumped_pos), header.BLACK_PIECE)[VERT]:
            return True, jumped_pos
        else:
            return False, pos


    def add_pos(self, pos1, pos2):
        return tuple(map(sum, zip(pos1, pos2)))



    # ================= PART B =================== #

    def massacre(self, enemy_team = header.BLACK_PIECE):
        # while enemy pieces exist:
            # find scores
            # move own pieces
            # remove dead enemies
        #

        has_enemy = True

        while has_enemy:
            has_enemy = False
            best_kill = None
            isFirst = True
            # This loop determines best kill
            for cur_square in self.board.list_pieces:
                if cur_square.square_t == enemy_team:
                    cur_square.score = self.find_score(cur_square)  # -> returns (avg_score, best_routes)
                    if isFirst:
                        isFirst = False
                        best_kill = cur_square
                    else:
                        #print(best_kill.score[SCORE], cur_square.score)
                        if best_kill.score[SCORE] > cur_square.score[SCORE]:
                            best_kill = cur_square

                    has_enemy = True

            # Now I need to move the best_kill

            if has_enemy == False:
                break

            # This shit UGLY
            print("\n==== NEW MOVE ====")
            # Piece 1
            piece1_pos = best_kill.score[PATH][0].path[0]
            goal1 = best_kill.score[PATH][0].path[-1]
            piece1 = self.get_square(piece1_pos)
            goal_piece1 = self.get_square(goal1)

            self.board.move_piece(piece1, goal_piece1)
            self.board.update()
            print("Move: ", piece1.pos, "->", goal_piece1.pos)

            # Piece 2
            # Only need this if we have 2 pieces to kill

            if len(best_kill.score[PATH]) > 1:
                piece2_pos = best_kill.score[PATH][1].path[0]
                goal2 = best_kill.score[PATH][1].path[-1]
                piece2 = self.get_square(piece2_pos)
                goal_piece2 = self.get_square(goal2)

                self.board.move_piece(piece2, goal_piece2)
                self.board.update()
                print("Move: ", piece2.pos, "->", goal_piece2.pos)









            print("\nUPDATED BOARD: \n ")
            header.print_board(self.board.dict_board)

    def assign_scores(self):
        # iterate through every piece in the board and assign their scores
        for cur_square in self.board.list_pieces:
            cur_square.score = self.find_score(cur_square)

    def find_score(self, cur_piece):
        # returns a score for the given piece, calculated #
        #  by finding the fastest path from every         #
        #  adjacent square to every opposing piece        #

        # find all routes to kill the given enemy piece, and sort them by their scores #
        routes = self.best_kill(cur_piece)

        # return worst score if no route is possible #
        if routes == None:
            return header.MAX_DIST, []

        routes.sort(key=lambda route: route.score)

        # find the 2 best routes from different pieces that can attack this piece #
        best_routes = [routes[0]]
        # find the first route with a different start-point and end-point to be route 2#
        for route in routes:
            if route.path[0] != routes[0].path[0] and\
                    route.path[-1] != routes[0].path[-1]:
                best_routes.append(route)
                break

        # find the sum of all route scores #
        total_score = 0
        for route in best_routes:

            total_score += route.score

        avg_score = total_score / len(best_routes)
        
        return (avg_score, best_routes)

    # TODO: need to change this to iterative deepening search
    # if we dont set the depth to max, it may fail due to needing to take a longer path than the direct one
    def dl_search(self, start_square, goal_square):
        """
        search for a path to a place, if a path is found, return
         a score, rather than the path
         perhaps return (pos_one, goal_pos, score)

         no no we need the path for output. return ([path], score)?
        """

        pos_one = start_square.pos
        goal_pos = goal_square.pos
        
        # check if piece is already in position #
        if pos_one == goal_pos:
            return 0, [pos_one]

        # check if square cannot be moved to #
        if goal_square.square_t != '-':
            return header.MAX_DIST, None

        # find the depth limit #
        limit = header.MAX_DIST - 4 #the 4 corner pieces, cant use find_dist cuz it limits the seach to direct paths
        # constant final node to indicate end of depth #
        TERMINAL_NODE = None

        stack = [pos_one]
        path = []
        visited = [pos_one]
        score = header.MAX_DIST

        while stack:
            # get current searching position #
            cur_pos = stack.pop()
            #print(cur_pos)
            #print(limit)
            # goal found; return constructed path #
            if cur_pos == goal_pos:
                path.append(cur_pos)
                score = len(path)-1

                return score, path

            # terminus of this level reached; go back one depth #
            elif cur_pos == TERMINAL_NODE:
                limit += 1
                visited = []
                path.pop()
                #print("==terminus reached==")

            # limit reached; increase limit, create new terminus, add new #
            #  possible moves to stack                                    #
            elif limit != 0:
                limit -= 1
                path.append(cur_pos)
                visited.append(cur_pos)
                stack.append(TERMINAL_NODE)
                stack.extend(self.find_new_moves(cur_pos, visited))
                #print("==limit increased==")

        print("Limit was reached")

    def find_new_moves(self, pos, visited):
        # finds all possible moves from the given position, either by #
        #  standard direct move, or by jumping                        #
        # returns a list containing all positions that have not       #
        #  been visited, and can be moved to without suicide          #

        new_moves = []
        for direction in header.DIRECTIONS:
            poss_move = self.check_move(pos, direction)

            # if direction can be moved in, add it to new_moves #
            if (poss_move[header.FLAG] and
                poss_move[header.POSITION] not in visited):
                new_moves.append(poss_move[header.POSITION])
        return new_moves

    def best_kill(self, cur_piece):
        # find if a piece is best killed horizontally or vertically, and the #
        #  routes to do it #

        routes = []
        # (bool, bool) representing whether to kill this piece horiz or vert #
        kill_status = self.check_sides(cur_piece)

        # piece cannot be killed #
        if kill_status[HOZ] == False and kill_status[VERT] == False:
            return None

        # ((pos, pos), (pos, pos)) representing all adjacent positions #
        sides = self.find_sides(cur_piece)
    
        if kill_status[HOZ] == True:

            # piece can only be killed horizontally #
            if kill_status[VERT] == False:
                # target the horizontal squares #
                target_pos = sides[HOZ]


            # piece can be killed from either side #
            else:
                target_pos = []
                target_pos.extend(sides[HOZ])
                # target_pos.extend(sides[VERT])

        # piece can only be killed vertically #
        else:
            target_pos = sides[VERT]

        # find the routes from each opposing piece to the two best #
        #  adjacent squares to target to kill this piece #
        self.board.find_pieces() # update piece list
        for other_piece in self.board.list_pieces:
            if other_piece.square_t != cur_piece.square_t:
                """
                print(other_piece.square_t, other_piece.pos, end=" ")
                print(cur_piece.square_t, cur_piece.pos)
                """
                for pos in target_pos:
                    #print(pos)
                    search = self.dl_search(other_piece, self.get_square(pos))
                    if search[PATH] != None:
                        routes.append(Route.Route(search[SCORE], search[PATH]))

            # TODO: determine kill orientation if both are accessible








        return routes

    def get_square(self, pos):
        return self.board.dict_board[pos]

    def find_sides(self, cur_piece):
        # find the (horizontal, vertical) positions adjacent to this square

        # find horizontal positions #
        horizs = [self.add_pos(cur_piece.pos, header.LEFT),
        self.add_pos(cur_piece.pos, header.RIGHT)]

        # find vertical positions #
        verts = [self.add_pos(cur_piece.pos, header.UP), 
        self.add_pos(cur_piece.pos, header.DOWN)]

        # change values to None if positions are out of bounds #
        for i in range(len(horizs)):
            if not self.board.check_bounds(horizs[i]):
                horizs[i] = None
            if not self.board.check_bounds(verts[i]):
                verts[i] = None

        return tuple(horizs), tuple(verts)

    def check_sides(self, cur_piece):
        # finds if a piece can be killed vertically and horizontally, returns #
        #  a tuple of booleans #

        # all adjacent positions #
        sides = self.find_sides(cur_piece)

        verts = sides[VERT]
        for pos in verts:
            v_flag = False

            # if a vert position is not valid, or is taken by a piece of the same #
            #  type as this one, this piece cannot be killed vertically #
            if pos == None:
                break
            elif self.get_square(pos).square_t == cur_piece.square_t:
                break
            else:
                v_flag = True
        
        horizs = sides[HOZ]

        for pos in horizs:
            h_flag = False

            # if a horiz position is not valid, or is taken by a piece of the same #
            #  type as this one, this piece cannot be killed horizontally #
            if pos == None:
                break
            elif self.get_square(pos).square_t == cur_piece.square_t:
                break
            else:
                h_flag = True
        
        return h_flag, v_flag

    """
    def check_death(self, piece):
        sides = self.find_sides(piece.pos)
        hoz = sides[HOZ]
        vert = sides[VERT]

        dead = True

        enemy = (header.BLACK_PIECE if piece.square == header.WHITE_PIECE else  header.WHITE_PIECE)

        for pos in hoz:
            square_t = self.get_square(pos).square_t
            if (square_t != enemy) or (square_t != header.CORNER):
                return not dead

        for pos in vert:
            square_t = self.get_square(pos).square_t
            if (square_t != enemy) or (square_t != header.CORNER):
                return not dead


        return dead
    """


    def find_distance(self, pos_one, pos_two):
        # finds the manhattan distance between two positions on the board #
        #  manhattan distance for (a,b) from (c,d) is |a-c| + |b-d|       #

        distance_x = abs(pos_one[header.X] - pos_two[header.X])
        distance_y = abs(pos_one[header.Y] - pos_two[header.Y])

        return distance_x + distance_y