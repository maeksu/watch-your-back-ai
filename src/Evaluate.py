
from header import X,Y
import random
import Route

SCORE = 0
PATH = 1
CENTER = [(3,3), (3,4), (4,3), (4,4)]


def evaluate_board(node):
    # returns a number representing roughly how positive a #
    #  board configuration is for a given team             #
    board = node.board
    team = node.team

    # how good the board is for me #
    my_pieces = 0
    # how good the board is for my enemy #
    enemy_pieces = 0

    pieces_from_center = 0

    for piece in board.list_pieces:

        if piece.square_t == team:
            # my_score += evaluate_piece(board, piece)
            my_pieces += 1
            pieces_from_center -= min([find_distance(piece.pos, x) for x in CENTER])
        else:
            # enemy_score += evaluate_piece(board, piece)
            enemy_pieces += 1

    # whatever number is found - control for number of pieces still in play #
    #  i.e. we want to make sure killing an enemy is ranked very high #
    weight = 0.33


    score = (my_pieces - enemy_pieces) + (weight * pieces_from_center)
    return score

def find_distance(pos_one, pos_two):
    # finds the manhattan distance between two positions on the board #
    #  manhattan distance for (a,b) from (c,d) is |a-c| + |b-d|       #

    distance_x = abs(pos_one[X] - pos_two[X])
    distance_y = abs(pos_one[Y] - pos_two[Y])

    return distance_x + distance_y
