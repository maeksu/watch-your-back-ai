import random
import Evaluate

class Node:
    def __init__(self, board, parent=None, move=None, team=None, turn=0):
        self.board = board                  # Board object/state
        self.team = team                    # Current team that is moving
        self.h_value = self._h_function()   # Heuristic value
        self.MM_value = None                # Value for MM
        self.parent = parent                # Parent Node
        self.children = []                  # Next set of possible moves
        self.move = move                    # Move that reached this state
        self.n_turns = turn                 # Total turns elapsed in the game

    def __str__(self, level=0):
        ret = "\t" * level + repr(self.h_value) + "\n"
        for child in self.children:
            ret += child.__str__(level + 1)
        return ret

    def _h_function(self):
        return Evaluate.evaluate_board(self)

    def add_node(self, new_node):
        self.children.append(new_node)
        return new_node

    def is_leaf(self):
        if len(self.children) == 0:
            return True
        else:
            return False
