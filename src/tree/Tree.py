from tree import Node

SUCCESS = 1
FAIL = 0


class Tree:

    def __init__(self, node):
        self.root = node

    def print_tree(self):
        print(self.root)
