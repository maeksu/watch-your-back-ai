import AI
import Board
from header import *
from random import randrange
import random
import sys

FORFEIT_LIMIT = 12 * 4

class RandomAI(AI.AI):

    # def __init__(self, board: Board, team):
    #     self.board = board
    #     self.team = team

    #     if team == WHITE_PIECE:
    #         self.seed = 6835615746476449351
    #     else:
    #         self.seed = 4392030331435138426

    #     random.seed(self.seed)

    def move(self):
        n_forfeit = 0
        
        while(True):
            n_forfeit += 1

            team_list = self.board.filter_pieces(self.team)

            if len(team_list) == 0:
                return
            
            index = randrange(len(team_list))
            piece = team_list[index]
            original_pos = piece.pos

            while(True):
                i = randrange (len (DIRECTIONS))
                new_pos = self.board.add_pos(piece.pos, DIRECTIONS[i])
                if(self.board.check_bounds(new_pos)):
                    break

            moveto = self.board.get_square(new_pos)

            if moveto.square_t == SPACE:
                self.board.move_piece(piece, moveto)
                break

            if n_forfeit == FORFEIT_LIMIT:
                return

        # TODO: Remove this comment before submission
        # self.board.print_board()

        return original_pos, new_pos

    def place(self):

        while(True):
            x = randrange(self.board.board_size[1]+1)
            y = randrange(self.board.board_size[1]+1)

            # Update board
            if self.check_placement((x,y)) and self.board.check_empty((x,y)):
                self.board.place_piece((x,y), self.team)
                break

        # TODO: Remove this comment before submission
        # self.board.print_board()

        return x, y