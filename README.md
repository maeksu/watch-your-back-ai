AI program for 'Watch Your Back!', employing minimax algorithm with alpha-beta pruning. Move analysis is based on
 knowledge of the ruleset, and a csv log of a thousand test games indicating past successful moves.

Game can be run using the included referee.py file, with each player assigned an AI strategy
 (either minimax or random placement).